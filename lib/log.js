var path = require('path')
var fs = require('fs')
var async = require("async")
var cluster = require('cluster');

module.exports = function (application) {
    this._urlRewrite = []
    this._application = application;
}

module.exports.prototype.router = function (req, res, next) {

    this.req = req;
    this.res = res;
    res.model = {}
    var then = this;

    for (var i = 0; i < this._urlRewrite.length; i++) {

        if(typeof this._urlRewrite[i][0] == "object"){

            if (this._urlRewrite[i][0].test(req.url)) {
                req.url = "./" + this._urlRewrite[i][1]
            }
        }else{
            if (req.url == this._urlRewrite[i][0]) {
                req.url = "./" + this._urlRewrite[i][1]
            }
        }
    }

    var action = null;
    var aUrl = req.url.split(path.sep)
    aUrl.splice(0, 1)
    aUrl.splice(1, 0, "controllers")
    for (var i = 0; i < aUrl.length; i++) {
        if ("" == aUrl[i]) {
            aUrl[i] = "index"
        }
        if (/:/i.test(aUrl[i])) {
            var _ac = aUrl[i].match(/(.*):(.*)/)
            aUrl[i] = _ac[1]
            action = _ac[2].split("?")[0]
        }
        if (/\?/i.test(aUrl[i])) {
            var _ac = aUrl[i].match(/(.*)?\?/)
            aUrl[i] = _ac[1]
        }
    }
    this.module = aUrl[0]

    var controllerPath = __RootPath + "node_modules/" + aUrl.join("/") + ".js";

    if(/\.css/ig.test(req.url) || /\.js/ig.test(req.url) || /\.png/ig.test(req.url) || /\.jpg/ig.test(req.url) || /\.jpeg/ig.test(req.url) || /\.gif/ig.test(req.url) || /\.ico/ig.test(req.url) || /null/ig.test(req.url) ){

        this.res.writeHead(404, {'contenttype': 'text/plain'});
        this.res.end("404");

        return false;
    }

    async.series([
            function(callback){

                // 判断控制器是否存在
                if(!fs.existsSync(controllerPath)){

                    then.res.writeHead(404, {'contenttype': 'text/plain'});
                    then.res.end("404")

                    system.log["console"].error("CPU:"+ cluster.worker.id, "-", req.headers.host,"- -"
                        , req.method
                        , req.url
                        ,res.statusCode
                        ,req.headers['user-agent'])

                    callback({err:1},null)
                }else{
                    system.log["console"].info("CPU:"+ cluster.worker.id, "-", req.headers.host,"- -"
                        , req.method
                        , req.url
                        ,res.statusCode
                        ,req.headers['user-agent'])

                    callback(null)
                }
            },
            function(callback){

                // 运行控制器
                then.runController(controllerPath, action, function(html){
                    callback(null,html)
                })
            }
        ],

        function(err, values){

            if (then.res.finished == false && values[1] != null) {
                then.res.end(values[1])
            }

            next()
        });
}



// url重写
module.exports.prototype.createUrlRewrite = function (url, newUrl) {
    this._urlRewrite.push([url, newUrl])
}


// 载入控制器、解析ext、组装模板
module.exports.prototype.runController = function (controllerPath, action, callback) {

    var html = ""

//    try {
        var controller = require(controllerPath);
//    } catch (e) {
//        system.log["console"].error("CPU:"+ cluster.worker.id, "-", e.message, "From: "+this.req.url)
//        this.res.writeHead(404, {'contenttype': 'text/plain'});
//        this.res.end("404")
//        callback()
//        return;
//    }

    var then = this;

    async.series([
            function(callback){

                then.res.html = callback;

                if (action) {
                    controller = controller.actions[action]
                }

                controller.application = then._application;
                controller.process(then.req, then.res);
                if (controller.view == null) {
                    return null;
                }

            }
        ],
        function(results){

            if (then.res._headerSent) {
                return;
            }

            // layout
            if (controller.layout) {
                var _controllerPath = __RootPath + "node_modules/" + then.module + "/controllers/" + controller.layout + ".js";

                then.runController(_controllerPath, false, function(_html){
                    html += _html
                })
            }

            var _html = ""
            if (controller.view) {

                try {
                    _html = then.req.ejs.render(fs.readFileSync(controllerPath.match(/(.*)controllers/i)[1] + controller.view).toString(), results)
                } catch (err) {
                    callback(err.message)
                    system.log["console"].error(err.message);
                }


                if (/\<view/.test(html)) {
                    html = html.replace(/<view.*>?/, "\n<!-- start layout -->\n\n" + _html + "\n\n<!-- stop layout -->\n")
                } else {
                    html = _html
                }
            }

            callback(html)
        });
}
