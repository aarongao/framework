var mongodb = require('mongodb')
var MongoClient = mongodb.MongoClient;


exports.connect = function (config, callback) {

    if (config && config.server) {

        if(config.username && config.password){

            var linkuri = 'mongodb://'+config.username+':'+config.password+'@' + config.server + ':' + config.port + '/' + config.name;
        }else{
            var linkuri = 'mongodb://' + config.server + ':' + config.port + '/' + config.name;
        }

        MongoClient.connect(linkuri, function (err, db) {
            if (err) {
                callback(err)
            }else{

                global.mongodb = mongodb;
                callback(null,db)
            }

        })
    }else{
        callback(null)
    }

}
