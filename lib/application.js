var connect = require('connect')
var http = require('http')
var log4js = require('log4js');
var serveStatic = require('serve-static')
var fs = require('fs')
var path = require('path')
var ejs = require('ejs');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var events = require("events");
var router = require('./router.js')
var format = require('util').format;
var formidable = require('formidable');
var util = require('util');
var qs = require("qs");
var async = require("async");
var cluster = require('cluster');

// 初始化全局变量
global.__RootPath = path.join(__dirname, "../../../")
global.log = console.log
global.system = {}


module.exports = function (config, cb) {

    this.connect = connect();
    this.modules = [];
    this.config = config
    this.event = new events.EventEmitter();

    // log4js
    config.log4js = {
        "appenders": [
            {
                "type": "console"
            }
            ,
            {
                "type": "file",
                "filename": "logs/access.log",
                "category": "access",
                "maxLogSize": 1024000,
                "backups": 4
            }
            ,
            {
                "type": "file",
                "filename": "logs/system.log",
                "category": "system",
                "maxLogSize": 1024000,
                "backups": 4
            }
            ,
            {
                "type": "file",
                "filename": "logs/console.log",
                "category": "console",
                "maxLogSize": 1024000,
                "backups": 4
            }
        ],
        "replaceConsole": true,
        "levels":{
            "access": "fatal",
            "system": "INFO",
            "console":"INFO"
        }
    }
    log4js.configure(config.log4js);

    var log4 = {}
    for(var i=0 ; i<config.log4js.appenders.length ; i++){
        if(config.log4js.appenders[i].type != 'console'){
            var logger = log4js.getLogger(config.log4js.appenders[i].category)
            this.connect.use(log4js.connectLogger(logger, { level: "auto" }));
            log4[config.log4js.appenders[i].category] = logger
        }
    }
    global.system.log = log4;
    global.system.print = global.system.log.system

    // cookies
    this.connect.use(cookieParser('secret string'))

    // session
    this.connect.use(cookieSession({
        keys: ['secret1', 'secret2'], maxage: new Date().getTime() + 3600 * 24 * 1000
    }))

    // ejs
    this.connect.use(function (req, res, next) {
        req.ejs = ejs;
        next()
    })

    this.connect.use(function (req, res, next) {

        req.query = {}

        if (req.method.toLowerCase() == 'post') {
            var form = new formidable.IncomingForm();
            form.parse(req, function (err, fields, files) {
                err && log(err)
                req.query = fields
                req.files = files

                var query = ~req.url.indexOf('?')
                    ? qs.parse(req._parsedUrl.query)
                    : {};

                extend(req.query, query)
                next()
            });
        } else {

            req.query = ~req.url.indexOf('?')
                ? qs.parse(req._parsedUrl.query)
                : {};
            next()
        }

    })


    // 注册模块目录
    var modulesPath = fs.readdirSync("./node_modules");
    for (var i = 0; i < modulesPath.length; i++) {
        if (modulesPath[i] != "framework" && !/^\./.test(modulesPath[i])) {
            this.modules.push(modulesPath[i])
        }
    }

    // 注册静态地址
    this.connect.use(serveStatic(__RootPath + "public"));
    this.connect.use(serveStatic(__RootPath + "bower_components"));
    for (var i = 0; i < this.modules.length; i++) {
        this.connect.use("/" + this.modules[i], serveStatic(__RootPath + "node_modules/" + this.modules[i] + "/bower_components"));
        this.connect.use("/" + this.modules[i], serveStatic(__RootPath + "node_modules/" + this.modules[i] + "/public"));
    }

    // 设置头信息
    this.connect.use(function(req, res, next){

        res.setHeader("Content-Type", "text/html") ;
        res.setHeader("Power-by", "Aaron-a") ;

        next()
    });


    async.series([
            function(callback){

                // 路由
                this.router = new router(this);
                callback()

            }.bind(this)

            , function(callback){

                // 数据库
                var db = require('./db.js');
                db.connect(config.db, function(err,db){

                    if(!err){
                        global.db = db;
                    }
                    callback(err)
                })


            }.bind(this)

            , function(callback){

                // extension
                for (var i = 0; i < this.modules.length; i++) {
                    var _extPath = __RootPath + "node_modules/" + this.modules[i] + "/extension.js";
                    if (fs.existsSync(_extPath)) {
                        var _extension = require(_extPath);
                        _extension.module = this.modules[i]
                        if (typeof _extension == "object") _extension.onload(this);
                    }
                }

                callback(null)
            }.bind(this)

            , function(callback){

                this.connect.use(this.router.router.bind(this.router))
                callback(null)
            }.bind(this)

        ],
        function(err, values){

            cb(err,this)

        }.bind(this));

}


// 开始监听端口
module.exports.prototype.start = function () {

    http.createServer(this.connect).listen(this.config.server.port, function () {
        system.print.info("CPU:" + getCpu() + ' - 框架启动完成！');
    })
}

function extend(o,n,override){
    for(var p in n)if(n.hasOwnProperty(p) && (!o.hasOwnProperty(p) || override))o[p]=n[p];
};


function getCpu(){
    if(cluster.worker){

        return cluster.worker.id;
    }else{
        return 0;
    }
}