#使用说明
此框架是为了使PHP开发者尽快转向NODE.JS而开发的。尽量保留了PHP MVC的开发模式。

## 目录

* / 项目根目录
* /config.json 配置文件
* /index.js 启动文件
* /public 静态目录。
    * ./images 图片
    * ./uploads 用户上传文件
    * ......
* bower_components 同public ，bower管理
* /node_modules 模块目录
    * ./framework 本框架（必须）
        * ./controllers 控制器目录
        * ./views 模板目录
        * extension.js 模块的启动文件
    * ./admincp 管理后台
        * 同上
    * ./users 用户模块
        * 同上
    * ./www
    * ./weixin
    * ......

> 访问静态文件时不需要写入public。如http://www.abc.com/images/abc.png



## 全局变量

* __RootPath 项目所在的绝对路径
* log 同console.log
* req.query 浏览器传来的参数对象
* req.files 上传文件参数
* res.end 输入参数内容到浏览器
* res.html 组装数据和模板后显示到浏览器，参数为数据对象
* req.ejs



## 控制器
同PHP中的控制器意思想同
~~~
module.exports = {

    layout: "layout",
    view: "views/index.html",

    process: function (req, res) {
        res.html({title:"hello"})
    }
    , actions: {
             list: {
                 layout: null,
                 view: "views/index_search.html",
                 process: function (req, res) {

                 }
             }
     }
~~~

### process
入口，打开网页时执行。
> 运算后必须使用res.end或res.html输出到浏览器，否则一直等待。

### actions
同process，一般用于子功能处理。
> 访问方式为 http://www.abc.com/news/index:list


### view
所使用的模板文件路径

### layout
指定一个公共部分控制器。
当前控制器会显示在layout控制器指定的区域。
> 指定方法为 <view/> 标记


## 关于extension
在node.js 启动时自动运行。做用为初始化数据，动作等信息。
如某模块需要监听特殊端口，或微信监听特殊关键词。

~~~
module.exports = {

    onload: function (application) {

        application.router.createUrlRewrite("/", "huawei.honor/index")

    }

} ;
~~~

## 关于application

记录了项目信息，如
* appliaction.config 配置文件中的信息
* application.router.createUrlRewrite 创建一个路由改写。如"createUrlRewrite("/", "huawei.honor/index")"意思是打开首页实际是访问"huawei.honor/index"控制器。
