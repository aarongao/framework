var application = require('./lib/application.js');
var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

exports.createApplication = function (config, cb) {

    if (cluster.isMaster && config.dev == false) {

        for (var i = 0; i < numCPUs; i++) {
            cluster.fork();
        }

        cluster.on('listening', function (worker, address) {
            //console.log('[master] ' + 'listening: worker' + worker.id + ',pid:' + worker.process.pid + ', Address:' + address.address + ":" + address.port);
        });
        cluster.on('disconnect', function (worker) {
            console.log('[master] ' + 'disconnect: worker' + worker.id);
        });

        cluster.on('exit', function (worker, code, signal) {
            console.log('[master] ' + 'exit worker' + worker.id + ' died');
        });

    } else {

        new application(config,function(err,application){

            err && system.log["system"].error(err)
            cb(null, application)
        });
    }

}








